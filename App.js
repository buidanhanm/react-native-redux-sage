/* 1 : navigate
 2: get param
 3 update param
 4 back to update
 5 :type navigate
*/
import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import './src/CustomView';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import ListUserScreen from './src/screen/listUser/ListUserScreen';
import DetailUserScreen from './src/screen/user/DetailUserScreen';
import AddUserContainer from './src/screen/user/addUser/AddUserContainer';
import {store} from './src/redux/configureStore';
import {Provider} from 'react-redux';

const Stack = createStackNavigator();

class App extends Component<State, Props> {
    render() {
        return (
            <Provider store={store}>
                <NavigationContainer>
                    <Stack.Navigator
                        screenOptions={{
                            headerShown: false,
                        }}
                        initialRouteName={ROUTE_NAME.TAB_BAR}
                    >
                        <Stack.Screen
                            name={ROUTE_NAME.TAB_BAR}
                            component={TabBar}
                        />
                    </Stack.Navigator>
                </NavigationContainer>
            </Provider>
        );
    }
};

export default App;
export const ROUTE_NAME = {
    LIST_USER: 'LIST_USER',
    DETAIL_USER: 'DETAIL_USER',
    TAB_BAR: 'TAB_BAR',
    MAIN_STACK: 'MAIN_STACK',
};


const MainStack = createStackNavigator();

function MainStackScreen() {
    return (
        <MainStack.Navigator
            screenOptions={{
                headerShown: false,
            }}
            initialRouteName={ROUTE_NAME.LIST_USER}>
            <MainStack.Screen name={ROUTE_NAME.LIST_USER} component={ListUserScreen}/>
            <MainStack.Screen name={ROUTE_NAME.DETAIL_USER} component={DetailUserScreen}/>

        </MainStack.Navigator>
    );
}

const Tab = createBottomTabNavigator();

function TabBar() {
    return (
        <Tab.Navigator>

            <Tab.Screen
                options={{
                    title: 'List User',
                }}
                name="ListUserScreen" component={MainStackScreen}/>
            <Tab.Screen options={{
                title: 'Add User',
            }} name="AddUserScreen" component={AddUserContainer}/>
        </Tab.Navigator>
    );
}
