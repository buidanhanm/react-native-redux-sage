import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';


class CustomButton extends React.Component {

    render() {

        return (
            <View>
                <TouchableOpacity onPress={() => {
                    // undef
                    if (this.props.onPress != null) {
                        this.props.onPress();
                    }
                }}>
                    <View style={{
                        borderRadius: 10,
                        borderWidth: 1,
                        marginVertical: 20,
                        marginHorizontal: 20,
                        borderColor: 'black',
                        padding: 10,
                        alignItems: 'center',
                        backgroundColor: '#ffcc6c',
                        border: 10,
                    }}>
                        <Text style={{color: 'black'}}
                        >{this.props.textLabe}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

export default CustomButton;
