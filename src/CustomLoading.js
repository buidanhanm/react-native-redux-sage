import React from 'react';
import {
    Text,
    TouchableOpacity,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Pressable,
    View,
    ActivityIndicator,
} from 'react-native';

export const CustomLoading = ({isShow}) => {
    return (
        isShow ? <View style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'gray',
            // zIndex: 100,
            opacity: 0.6,
            position: 'absolute',
        }}>
            <ActivityIndicator size={'large'} color={'green'}/>
        </View> : null
    );
};
