import React, {Component} from 'react';
import {Text, View} from 'react-native';

class CustomView extends Component {
    constructor() {
        super();
        this.setState({
            value: 10,
        });
    }
    componentDidMount() {
        // call API
        console.log('============ componentDidMount  =========')
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('============ componentDidUpdate  =========');
    }

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        console.log('============ componentWillReceiveProps  =========');

    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('============ shouldComponentUpdate  =========', nextProps.value);
        if (nextProps.item !== this.props.item) {
            return true;
        }
        return true;
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <View>
                <Text style={{
                    marginLeft: 10,
                    color: 'blue',
                }}>Item {this.props.item}</Text>

                <Text style={{
                    marginLeft: 10,
                    color: 'blue',
                }}>Value {this.props.value}</Text>
            </View>
        );
    }
}

export default CustomView;
