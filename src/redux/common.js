export interface State {
    isLoading: boolean;
    data: any;
    isSuccess: boolean;
    errorMessage: string | null;
    dataInput: any;
}

export const initialState: State = {
    isLoading: false,
    data: null,
    isSuccess: false,
    errorMessage: null,
    dataInput: {},
};

class Epic {
    value: string;

    constructor(value: string) {
        this.value = value;
    }

    success = () => `${this.value}_SUCCESS`;
    fail = () => `${this.value}_FAIL`;
    clear = () => `${this.value}_CLEAR_STATE`;
    add = () => `${this.value}_ADD_DATA`;
    update = () => `${this.value}_UPDATE_DATA`;
}


export const EPIC = {
    GET_LIST_USER: new Epic('GET_LIST_USER'),
    ADD_USER: new Epic('ADD_USER'),
    EDIT_USER: new Epic('EDIT_USER'),
};
