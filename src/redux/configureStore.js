import {applyMiddleware, compose, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import rootReducers from './reducers';
import rootSaga from './sagas';


const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];

middleware.push(logger);

const store = createStore(rootReducers, compose(
    applyMiddleware(...middleware),
));

sagaMiddleware.run(rootSaga);

export {
    store,
};
