import {combineReducers} from 'redux';
import {addUserReducer, editUserReducer} from '../screen/user/Reducer';
import {listUserReducer} from '../screen/listUser/Reducer';

const rootReducers = combineReducers({
    addUserReducer,
    editUserReducer,
    listUserReducer,
});
export default rootReducers;
