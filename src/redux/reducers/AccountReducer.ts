import {accountTypes} from '../actions/types'

const initialState: any = {
    loading: false,
}
export default function (state = initialState, action: any) {
    switch (action.type) {
        case accountTypes.UPDATE_PROFILE:
            return Object.assign({}, state, {
                loading: true,
                infoUser: action.user_info,
            })
        case accountTypes.UPDATE_PROFILE_FAILED:
            return Object.assign({}, state, {
                loading: false,
                state_update_profile: accountTypes.UPDATE_PROFILE_FAILED,
                message: action.message,
            })
        case accountTypes.UPDATE_PROFILE_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                state_update_profile: action.state_update_profile,
            })
        case accountTypes.GET_PROFILE_FAILED:
            return Object.assign({}, state, {
                loading: false,
            })
        case accountTypes.GET_PROFILE_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                infoUser: action.infoUser
            })
        case accountTypes.GET_PROFILE:
            return Object.assign({}, state, {
                loading: true,
            })
        case accountTypes.RESET_STATE_PROFILE:
            return Object.assign({}, state, {
                state_update_profile: '',
            })
        default:
            return state
    }
}
