import {all} from 'redux-saga/effects';
import user from '../screen/user/Saga';
import listUser from '../screen/listUser/Saga';

export default function* rootSaga() {
    yield all([
        user(),
        listUser(),

    ]);
}
