import React, {useEffect} from 'react';
import {FlatList, TouchableOpacity, SafeAreaView, StatusBar, StyleSheet, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getListUser} from '../user/Acticon';
import {CustomLoading} from '../../CustomLoading';
import {ROUTE_NAME} from '../../../App';
import {store} from '../../redux/configureStore';

const ListUserScreen = (props) => {

    const listUserReducer = useSelector((state) => state.listUserReducer);

    // const listUserReducer = store.getState().listUserReducer;

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getListUser());
        return {
            // action acb
        }

    }, []);

    const renderItemUser = (user) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    props.navigation.navigate(ROUTE_NAME.DETAIL_USER,user);
                }}
            >
                <View style={{
                    margin: 7,
                }}>
                    <View>
                        <Text style={{
                            color: 'blue',
                        }}>Name : {user.name} </Text>
                        <Text style={{}}>ID : {user.id} </Text>
                        <Text>Age : {user.age}</Text>
                    </View>
                    <View
                        style={{
                            marginTop: 5,
                            width: '100%',
                            height: 1,
                            backgroundColor: 'gray',
                            color: 'red',
                        }}/>
                </View>
            </TouchableOpacity>
        );
    };

    return (<SafeAreaView style={{
            flex: 1,
        }}>
            <StatusBar barStyle={'dark-content'}/>
            <Text style={{
                marginBottom: 20,
                alignSelf: 'center',
                fontSize: 15,
                fontWeight: 'bold',
            }}>List User</Text>
            <FlatList
                onRefresh={() => {
                    dispatch(getListUser());
                }}
                refreshing={listUserReducer.isLoading}
                data={listUserReducer.data}
                renderItem={({item}) => renderItemUser(item)}
            />
            <CustomLoading isShow={listUserReducer.isLoading}/>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({});

export default ListUserScreen;

