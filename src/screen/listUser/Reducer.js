import {EPIC, initialState} from '../../redux/common';

const listUserReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case EPIC.GET_LIST_USER.value:
            return {
                ...state,
                isLoading: true,
                isSuccess: false,
                errorMessage: null,
                data: null,
            };
        case EPIC.GET_LIST_USER.success():
            return {
                ...state,
                isLoading: false,
                isSuccess: true,
                errorMessage: null,
                data: action.data,
            };
        case EPIC.GET_LIST_USER.add():
            console.log('============ listUserReducer  =========',action)
            state.data.push(action.data);
            return {
                ...state,
                isLoading: false,
                isSuccess: true,
                errorMessage: null,
                data: state.data,
            };
        case EPIC.GET_LIST_USER.update():
            // state.data.forEach(value => {
            //     if (value.id == action.data.id) {
            //         value = action.data;
            //     }
            // });

            // state.data.fo
            var index = state.data.findIndex((value => value.id == action.data.id));
            // const listUpdate = state.data.map(obj => {
            //         return obj.id == action.data.id ? {...action.data} : obj;
            //     },
            // );
            state.data[index] = action.data;
            console.log('============ id  =========', index);
            console.log('============ listUserReducer  =========', state.data);
            return {
                ...state,
                isLoading: false,
                isSuccess: true,
                errorMessage: null,
                data: state.data,
            };
        case EPIC.GET_LIST_USER.fail():
            return {
                ...state,
                isLoading: false,
                isSuccess: false,
                errorMessage: action.errorMessage,
                data: null,
            };
        case EPIC.GET_LIST_USER.clear():
            return initialState;
        default:
            return state;
    }
};

export {listUserReducer};
