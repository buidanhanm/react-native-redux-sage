import {all, call, fork, put, takeLatest, delay} from 'redux-saga/effects';
import {EPIC} from '../../redux/common';

const DATA_USER = [
    {name: 'Nguyen Van A', age: 11, id: 1},
    {name: 'Bui Quang C', age: 13, id: 2},
    {name: 'Le thi D', age: 13, id: 3},
];

function* getListUser() {
    yield takeLatest(EPIC.GET_LIST_USER.value, function* (action: any) {
        try {
            yield delay(1000);
            yield put({
                type: EPIC.GET_LIST_USER.success(),
                data: DATA_USER,
            });


        } catch (e) {
            yield put({
                type: EPIC.GET_LIST_USER.fail(),
                errorMessage: e.toString(),
            });
        }
    });
}


export default function* () {
    yield all([
        fork(getListUser),
    ]);
}
