import {EPIC} from '../../redux/common';

const addUser = (dataInput: any) => {
    return {
        type: EPIC.ADD_USER.value,
        dataInput : dataInput,
    };
};
const editUser = (dataInput: any) => {
    return {
        type: EPIC.EDIT_USER.value,
        dataInput,
    };
};
const editUserReset = () => {
    return {
        type: EPIC.EDIT_USER.clear(),
    };
};
const getListUser = () => {
    return {
        type: EPIC.GET_LIST_USER.value,
    };
};
export {addUser, editUser, getListUser, editUserReset};
