import React, {useEffect, useState} from 'react';
import {Alert, SafeAreaView, StatusBar, StyleSheet, Text, TextInput} from 'react-native';
import {CustomLoading} from '../../CustomLoading';
import CustomButton from '../../CustomButton';
import {useDispatch, useSelector} from 'react-redux';
import {editUser, editUserReset} from './Acticon';

const DetailUserScreen = (props) => {
    const editUserReducer = useSelector((state) => state.editUserReducer);
    const dispatch = useDispatch();

    const dataUser = props.route.params;
    const [name, setName] = useState(dataUser.name);
    const [age, setAge] = useState(dataUser.age);

    useEffect(() => {
        if (editUserReducer.isSuccess) {
            Alert.alert(
                'Thông báo',
                'Cập nhật thông tin thành công !',
                [

                    {
                        text: 'OK', onPress: () => {
                            props.navigation.goBack();
                            dispatch(editUserReset());
                        },
                    },
                ],
            );
        } else if (editUserReducer.errorMessage) {
            Alert.alert(
                'Error',
                this.props.errorMessage,
                [
                    {text: 'OK'},
                ],
            );
        }
    }, [editUserReducer]);
    return (<SafeAreaView style={{
            flex: 1,
        }}>
            <StatusBar barStyle={'dark-content'}/>
            <Text style={{
                marginBottom: 20,
                alignSelf: 'center',
                fontSize: 15,
                fontWeight: 'bold',
            }}>Edit User</Text>
            <TextInput
                placeholder="Input Name"
                style={styles.input}
                onChangeText={(text) => {
                    setName(text);
                }}
                value={name + ''}
            />
            <TextInput
                style={styles.input}
                onChangeText={(text) => {
                    setAge(text);
                }}
                placeholder="Input Age"
                keyboardType="numeric"
                value={age + ''}
            />
            <CustomButton textLabe={'Update User'} onPress={() => {
                dispatch(editUser({...dataUser, age: age, name: name}));
            }}/>
            <CustomButton textLabe={'Delete User'} onPress={() => {
                dispatch(editUser({...dataUser, age: age, name: name}));
            }}/>
            <CustomLoading isShow={editUserReducer.isLoading}/>
        </SafeAreaView>
    );
};
const styles = StyleSheet.create({

    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        paddingStart: 10,
    },
});

export default DetailUserScreen;

