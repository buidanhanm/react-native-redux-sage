import {EPIC, initialState} from '../../redux/common';

const addUserReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case EPIC.ADD_USER.value:
            return {
                ...state,
                isLoading: true,
                isSuccess: false,
                errorMessage: null,
                data: null,
                dataInput: action.dataInput,
            };
        case EPIC.ADD_USER.success():
            return {
                ...state,
                isLoading: false,
                isSuccess: true,
                errorMessage: null,
                data: action.data,
            };
        case EPIC.ADD_USER.fail():
            return {
                ...state,
                isLoading: false,
                isSuccess: false,
                errorMessage: action.errorMessage,
                data: null,
            };
        case EPIC.ADD_USER.clear():
            return initialState;
        default:
            return state;
    }
};
const editUserReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case EPIC.EDIT_USER.value:
            return {
                ...state,
                isLoading: true,
                isSuccess: false,
                errorMessage: null,
                data: null,
                dataInput: action.dataInput,
            };
        case EPIC.EDIT_USER.success():
            return {
                ...state,
                isLoading: false,
                isSuccess: true,
                errorMessage: null,
                data: action.data,
            };
        case EPIC.EDIT_USER.fail():
            return {
                ...state,
                isLoading: false,
                isSuccess: false,
                errorMessage: action.errorMessage,
                data: null,
            };
        case EPIC.EDIT_USER.clear():
            return initialState;
        default:
            return state;
    }
};

export {addUserReducer, editUserReducer};
