import {all, delay, fork, put, takeLatest} from 'redux-saga/effects';
import {EPIC} from '../../redux/common';
import {store} from '../../redux/configureStore';

function* addUser() {
    yield takeLatest(EPIC.ADD_USER.value, function* (action: any) {
        try {
            yield delay(1000);
            let age = action.dataInput.age;
            const name = action.dataInput.name.toString();
            if (age > 50) {
                yield put({
                    type: EPIC.ADD_USER.fail(),
                    errorMessage: 'Age need less than 50 years',
                });
            } else {
                yield put({
                    type: EPIC.ADD_USER.success(),
                    data: action.dataInput,
                });
                const listUser = store.getState().listUserReducer.data;
                const idNewUser = listUser.length + 1;
                yield put({
                    type: EPIC.GET_LIST_USER.add(),
                    data: {...action.dataInput, id: idNewUser},
                });
            }

        } catch (e) {
            yield put({
                type: EPIC.ADD_USER.fail(),
                errorMessage: e.toString(),
            });
        }
    });
}

function* editUser() {
    yield takeLatest(EPIC.EDIT_USER.value, function* (action: any) {
        try {
            yield delay(1000);
            let age = action.dataInput.age;
            const name = action.dataInput.name.toString();
            if (age > 50) {
                yield put({
                    type: EPIC.EDIT_USER.fail(),
                    errorMessage: 'Age need less than 50 years',
                });
            } else {
                yield put({
                    type: EPIC.EDIT_USER.success(),
                    data: action.dataInput,
                });
                yield put({
                    type: EPIC.GET_LIST_USER.update(),
                    data: action.dataInput,
                });
            }

        } catch (e) {
            yield put({
                type: EPIC.ADD_USER.fail(),
                errorMessage: e.toString(),
            });
        }
    });
}

export default function* () {
    yield all([
        fork(addUser),
        fork(editUser),
    ]);
}
