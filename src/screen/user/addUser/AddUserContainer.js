import {connect} from 'react-redux';
import AddUserScreen from './AddUserScreen';
import {addUser} from '../Acticon';

const mapStateToProps = (state: any) => ({
    isLoading: state.addUserReducer.isLoading,
    isSuccess: state.addUserReducer.isSuccess,
    errorMessage: state.addUserReducer.errorMessage,
    data: state.addUserReducer.data,
});
const mapDispatchToProps = (dispatch: any) => ({
    addUser: (dataInput) => dispatch(addUser(dataInput)),
});
export default connect(
    mapStateToProps, mapDispatchToProps,
)(AddUserScreen);
