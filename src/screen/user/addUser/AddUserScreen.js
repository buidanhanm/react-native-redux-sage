import React, {Component} from 'react';
import {Alert, SafeAreaView, StatusBar, StyleSheet, Text, TextInput} from 'react-native';
import CustomButton from '../../../CustomButton';
import {CustomLoading} from '../../../CustomLoading';

class AddUserScreen extends Component {
    constructor() {
        super();
        this.state = {
            name: null,
            age: null,
        };
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        // const errorMessage = store.getState().addUserReducer.errorMessage;
        if (this.props.errorMessage != null && this.props.errorMessage !== prevProps.errorMessage) {
            Alert.alert(
                'Error',
                this.props.errorMessage,
                [
                    {text: 'OK'},
                ],
            );
        }
        if (this.props.isSuccess === true && this.props.isSuccess !== prevProps.isSuccess) {
            Alert.alert(
                'Thông báo',
                'Thêm user thành công',
                [
                    {text: 'OK'},
                ],
            );
        }
    }
    componentWillUnmount(): void {
        //action abc
    }

    render() {
        console.log('============ render  =========',this.props)
        return (<SafeAreaView style={{
                flex: 1,
            }}>
                <StatusBar barStyle={'dark-content'}/>
                <Text style={{
                    marginBottom: 20,
                    alignSelf: 'center',
                    fontSize: 15,
                    fontWeight: 'bold',
                }}>Add User</Text>
                <TextInput
                    placeholder="Input Name"
                    style={styles.input}
                    onChangeText={(text) => {
                        this.setState({
                            name: text,
                        });
                    }}
                />
                <TextInput
                    style={styles.input}
                    onChangeText={(text) => {
                        this.setState({
                            age: text,
                        });
                    }}
                    placeholder="Input Age"
                    keyboardType="numeric"
                />
                <CustomButton textLabe={'Add User'} onPress={() => {
                    this.props.addUser({
                        name: this.state.name,
                        age: this.state.age,
                    });
                }}/>
                <CustomLoading isShow={this.props.isLoading}/>
            </SafeAreaView>
        );
    }

};

const styles = StyleSheet.create({

    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        paddingStart: 10,
    },
});

export default AddUserScreen;

